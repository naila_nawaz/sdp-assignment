import { FlyBehavior } from '../flyBehavior/flyBehavior';
import { QuackBehavior } from '../quackBehavior/quackBehavior';

export class Duck {
  name: string;
  // Program to Interfance
  // Below two class members will be initialized with the relevant behavior's classes at run time
  flyBehavior: FlyBehavior;
  quackBehavior: QuackBehavior;

  swim() {}

  display() {}

  performFly() {
    // depending on the assigned fly behavior, relevant's fly behavior class method will be called
    this.flyBehavior.fly();
  }

  performQuack() {
    // depending on the assigned quack behavior, relevant's quack behavior class method will be called
    this.quackBehavior.quack();
  }

  setFlyBehavior(fb) {
    // as per program to interface pattern, FlyBehavior interface member variable will take some fly behavior class
    // at run time and will be assigned to duck class through this method
    this.flyBehavior = fb;
  }

  setQuackBehavior(qb) {
    // as per program to interface pattern, QuackBehavior interface member variable will take some quack behavior class
    // at run time and will be assigned to duck class through this method
    this.quackBehavior = qb;
  }
}
