import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { Duck } from '../duck/duck';
import { FlyWithWings } from '../flyBehavior/flyWithWings';
import { FlyNoWay } from '../flyBehavior/flyNoWay';
import { Quack } from '../quackBehavior/quack';
import { Squeak } from '../quackBehavior/squeak';
import { MuteQuack } from '../quackBehavior/muteQuack';

@Component({
  selector: 'app-duck',
  templateUrl: './duck.component.html',
  styleUrls: ['./duck.component.css'],
})
export class DuckComponent implements OnInit {
  form: FormGroup;
  ducks: Duck[];

  constructor(private fb: FormBuilder) {
    // constructor

    // form to add a new duck along with its behaviors
    this.form = this.fb.group({
      name: [null, Validators.required],
      fly: ['', Validators.required],
      quackBehavior: ['', Validators.required],
    });

    // new ducks will be added in this array and this array will be shown in table view on front end
    this.ducks = [];
  }

  ngOnInit(): void {}

  addDuck() {
    // this method will be called to add/instantiate a new duck
    let duck = new Duck();
    duck.name = this.form.controls.name.value;

    let flyBehavior = this.form.controls.fly.value;

    // if the duck fly then FlyWithWings Behavior will be assigned
    if (flyBehavior == 'FlyWithWings') {
      duck.setFlyBehavior(new FlyWithWings());
    } else if (flyBehavior == 'NoFly') {
      // if the duck does notfly then FlyNoWays Behavior will be assigned
      duck.setFlyBehavior(new FlyNoWay());
    }

    let quackBehavior = this.form.controls.quackBehavior.value;
    // if the duck quack then quack Behavior will be assigned
    if (quackBehavior == 'quack') {
      duck.setQuackBehavior(new Quack());
    }
    // if the duck squeak then squeak Behavior will be assigned
    else if (quackBehavior == 'squeak') {
      duck.setQuackBehavior(new Squeak());
    }
    // if the duck doesn't quack or squeak then muteQuack Behavior will be assigned
    else if (quackBehavior == 'muteQuack') {
      duck.setQuackBehavior(new MuteQuack());
    }

    // after assigning name and both behaviors to the duck, add it in array
    this.ducks.push(duck);

    // reset form
    this.form.reset();
  }

  // this method will call the fly method of relevant class, which is assigned to the duck
  checkFly(duck) {
    duck.performFly();
  }

  // this method will call the quack method of relevant class, which is assigned to the duck
  checkQuack(duck) {
    duck.performQuack();
  }
}
