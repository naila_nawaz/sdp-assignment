/******************************************************************************/
/* This Class inherits QuackBehavior interface and                           */
/* implements the quack method                                              */
/* All the ducks that will be assigned this quack behavior can squeak      */
/********************************************** ***************************/

import { QuackBehavior } from "./quackBehavior";

export class Squeak implements QuackBehavior {

  // This quack method can be called to check the relevant quack behavior
  quack() {
    alert ("This Duck Squeak");
    console.log("Squeak - implements QuackBehavior");  }

}
