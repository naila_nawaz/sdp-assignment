// By using the quack behavior interface we can achive program to interface design strategy
export interface QuackBehavior {
  quack();
}
