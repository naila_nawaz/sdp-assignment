/******************************************************************************/
/* This Class inherits QuackBehavior interface and                           */
/* implements the quack method                                              */
/* All the ducks that will be assigned this quack behavior can quack       */
/********************************************** ***************************/

import { QuackBehavior } from "./quackBehavior";

export class Quack implements QuackBehavior {

  // This quack method can be called to check the relevant quack behavior
  quack() {
    alert ("This Duck Quack");
    console.log("Quack - implements QuackBehavior");
  }

}
