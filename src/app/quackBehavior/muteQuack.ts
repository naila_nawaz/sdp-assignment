/******************************************************************************/
/* This Class inherits QuackBehavior interface and                           */
/* implements the quack method                                              */
/* All the ducks that will be assigned this quack behavior can not quack   */
/********************************************** ***************************/

import { QuackBehavior } from "./quackBehavior";

export class MuteQuack implements QuackBehavior {

  // This quack method can be called to check the relevant quack behavior
  quack() {
    alert ("This Duck is Mute");
    console.log("mute quack - implements QuackBehavior");
  }

}
