// By using the fly behavior interface we can achive program to interface design strategy
export interface FlyBehavior {
  fly();
}
