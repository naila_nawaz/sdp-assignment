/*************************************************************************/
/* This Class inherits FlyBehavior interface and                        */
/* implements the fly method                                           */
/* All the ducks that will be assigned this fly behavior can fly      */
/********************************************** **********************/

import { FlyBehavior } from "./flyBehavior";

export class FlyWithWings implements FlyBehavior {

  // This fly method can be called to check the relevant fly behavior
  fly() {
    alert ("This duck can fly");
    console.log("FlyWithWings - implements FlyBehavior");
  }

}
