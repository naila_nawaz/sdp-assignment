/***********************************************************************/
/* This Class inherits FlyBehavior interface and                      */
/* implements the fly method                                         */
/* All the ducks that will be assigned this fly behavior do not fly */
/********************************************** ********************/
import { FlyBehavior } from "./flyBehavior";

export class FlyNoWay implements FlyBehavior {

  // This fly method can be called to check the relevant fly behavior
  fly() {
    alert ("This duck can not fly");
    console.log("FlyNoWay - implements FlyBehavior");
  }

}
